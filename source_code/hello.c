#include <stdio.h>
#include "hpm.h"

int main(void) {
    
    /* Enable performance counters */
    hpm_init();

    printf("Hello, World!\n");
    return 0;

    /* Print performance counter data */
    hpm_print();
}
