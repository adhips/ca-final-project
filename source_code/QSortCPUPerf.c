#include<stdio.h>
#include<stdlib.h>
#include "hpm.h"
int PartitionArr(int *Arr, int L, int H){
    int i=L-1;
    int temp;
    for(int j=L; j< H;j++){
        if(Arr[j]<Arr[H]){
            i++;
            temp=Arr[j];
            Arr[j]=Arr[i];
            Arr[i]=temp;
        }
    }
    temp=Arr[i+1];
    Arr[i+1]=Arr[H];
    Arr[H]=temp;
    return i+1;
}
void quickSort(int *Arr, int Low, int High){
    if(High<Low)
        return;
    else{
        int PivotFinal= PartitionArr(Arr, Low, High);
        quickSort(Arr,Low, PivotFinal-1);
        quickSort(Arr, PivotFinal+1, High);
    }
}
int main(){
    int Len=10000;
    int *Arr= malloc(sizeof(int)*Len);
    for(int i = 0; i < Len; i++) { 
        Arr[i] = (rand() % (Len*2 + 1)); 
    }
    hpm_init();
    quickSort(Arr, 0, Len-1);
    hpm_print();
    return 0;
}