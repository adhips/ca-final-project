#include<stdio.h>
#include<stdlib.h>
#include "hpm.h"
#define L1DCacheSize 4096
#define CacheLineSize 64
int main(){
    int *ScanArray=(int *)malloc(L1DCacheSize);
    int ScanArrLen = L1DCacheSize/sizeof(int);
    int *WorkArray=(int *)malloc(L1DCacheSize);
    int WorkArrLen = L1DCacheSize/sizeof(int);
    int ii ,i;
    int jump =CacheLineSize/sizeof(int);
    hpm_init();
    for (ii=0; ii< 12; ii++){
        if(ii%5 || ii==0){
            for(i=0; i<WorkArrLen; i=i+jump){//Jumping to new line
                WorkArray[i]++;
                if(ii!=0 && i==WorkArrLen/2){
                    break;//Access the first half of array(working set) of the cache twice to set the RRPV to 0
                }
            }
        }
        else{
            for(i=0; i<ScanArrLen; i+=jump){
                ScanArray[i]++;// Scaning all the elements in the array
            }
        }
    }
    hpm_print();
    return 0;
}